Nodewords Twitter: The Drupal 6 Meta Tags module for Twitter cards
------------------------------------------------------------------

This module is a support module for nodewords that allows you to set
meta tags required to create Twitter cards as detailed here:

https://dev.twitter.com/docs/cards

Twitter have also released a useful preview tool that allows you to
enter a link or paste in the meta tags to get an idea of how your expanded
tweets will look:

https://dev.twitter.com/docs/cards/preview


This version of the module only works with Drupal 6.x.

This module hooks into the API exposed by nodewords:

http://drupal.org/projects/nodewords

...and depends on this module to operate.
